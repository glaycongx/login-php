Sistema de Ponto Eletrõnico - Solides

O arquivo se trata de um sistema de registro de ponto, composto de validação via login.

Para executar o sistema, basta:

Baixar o repositório "login-php"

Adicioná-lo a pasta www, dentro da instalação do WAMP.

Pronto! Basta acessar: localhost/login-php

Obs: O banco de dados anexo já contém um login:

usuário: glaycon
senha:123

Considerações sobre o projeto:

O sistema hospedado é a versão sem MVC do projeto.

O repositório cujo projeto foi desenvolvido utilizando o CodeIgniter está disponível como: login-php-codeigniter.

Compartilhei as duas versão com o Frederico.

O sistema foi hospedado através do hostingers, sob o domínio: http://pontodoestagiario.tech/

Os reposítórios estão disponíveis no GIT HUB através dos links:

Sem MVC: https://github.com/Glaycongx/Solides.git

Com MVC: https://github.com/Glaycongx/login-php-codeigniter.git